# Usage

### Initialize Motor and Set Limits

    from piezo_motor import PiezoMotor
    motor = PiezoMotor("97101691", lower_limit=-10000, upper_limit=10000)

## Configure Limits

Set Absolute Limits

    motor.set_absolute_limits(-100000, 100000)

View Absolute Limits

    motor.get_absolute_limits() 

Set Relative Limits
    
    motor.set_relative_limits(-1000, 1000)

View Relative Limits

    motor.get_relative_limits()

## Moving the Motor

These can all specify channel with the parameter `channel=1`. They correspond to 
the motor number on the piezo controller. Default is channel 1. The default can 
be changed when initializing the motor with `default_channel=1`.

Move relative to current position - can be positive of negative
    
    motor.move_by(100)

Move to absolute position

    motor.move_to(0)

Sets current position as zero 

    motor.zero()

### Close the motor when done

    motor.close()
