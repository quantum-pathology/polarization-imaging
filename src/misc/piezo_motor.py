from pylablib.devices import Thorlabs
from typing import Any, Union, Tuple


class PiezoMotor(Thorlabs.KinesisPiezoMotor):
    """
    Subclass of KinesisPiezoMotor that implements drive limits.

    Thorlabs piezo motor (TIM/KIM series) controller.

    Implements FTDI chip connectivity via pyft232 (virtual serial interface).

    """

    def __init__(
        self,
        conn: str,
        lower_limit: int = 0,
        upper_limit: int = 0,
        default_channel: int = 1,
    ):
        super().__init__(conn=conn, default_channel=default_channel)
        self.set_relative_limits(lower_limit=lower_limit, upper_limit=upper_limit)
        self.setup_drive(velocity=1000, acceleration=1000)

    def get_relative_limits(self) -> Tuple[int, int]:
        """Returns the motor limits relative to current position."""
        current_position = self.get_position()
        return (
            self.lower_limit - current_position,
            self.upper_limit - current_position,
        )

    def get_absolute_limits(self) -> Tuple[int, int]:
        """Returns the absolution position of motor limits."""
        current_position = self.get_position()
        return self.lower_limit, self.upper_limit

    def set_relative_limits(self, lower_limit: int = 0, upper_limit: int = 0) -> None:
        """
        Sets custom bounds for the motor relative to current position.

        Args:
            lower_limit: Lower bound of motor position.
                e.g. -10000 means 10000 below current position.
            upper_limit: Lower bound of motor position.
                e.g. +10000 means 10000 above current position.
        """
        current_position = self.get_position()
        self.lower_limit = current_position + lower_limit
        self.upper_limit = current_position + upper_limit

    def set_absolute_limits(self, lower_limit: int, upper_limit: int) -> None:
        """
        Sets custom bounds for motor positions in absolute units.

        Args:
            lower_limit: Lower bound of motor position.
                e.g. -10000 means motor can't go below -10000.
            upper_limit: Lower bound of motor position.
                e.g. +10000 means motor can't go above 10000.
        """
        if lower_limit > upper_limit:
            raise ValueError("lower_limit cannot be greater than upper_limit")
        current_position = self.get_position()
        self.upper_limit = upper_limit
        self.lower_limit = lower_limit

    def move_by(
        self,
        distance=0,
        auto_enable: bool = True,
        channel: Union[Any, None] = None,
    ) -> None:
        current_position = self.get_position()

        if self.lower_limit < current_position + distance < self.upper_limit:
            return super().move_by(
                distance=distance, auto_enable=auto_enable, channel=channel
            )
        else:
            raise self.MotorOutOfBoundsError(self.lower_limit, self.upper_limit)

    def move_to(self, position: int, channel: Union[Any, None] = None):
        if self.lower_limit < position < self.upper_limit:
            return super().move_to(position=position, channel=channel)
        else:
            raise self.MotorOutOfBoundsError(self.lower_limit, self.upper_limit)

    def zero(self, channel=None):
        """Sets current position to zero."""
        return super()._set_position_reference(position=0, channel=channel)

    class MotorOutOfBoundsError(Exception):
        """Motor position outside of set limits."""

        def __init__(self, lower_limit, upper_limit) -> None:
            self.message = (
                "Must move motor within limits " f"({lower_limit}, {upper_limit})."
            )
            super().__init__(self.message)
