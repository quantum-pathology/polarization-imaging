import numpy as np
import matplotlib.pyplot as plt

arr1 = np.random.random((10, 10, 3))
arr2 = np.random.random((10, 10, 3))

arr1[0, 0, :] = np.nan

arr1[arr1 > 0.5] = np.nan

plt.imshow(arr1)
plt.colorbar()
plt.show()

print(arr1 * arr2)
