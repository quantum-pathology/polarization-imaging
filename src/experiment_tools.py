from __future__ import annotations
import numpy as np
import numpy.typing as npt
import matplotlib.pyplot as plt
import pandas as pd
import os
from PIL import Image
from skimage import io
import time
from datetime import date
from IPython import display as dis
from instrumental import u, errors
import instrumental.drivers.cameras.uc480 as uc480
from thorlabs_apt.core import Motor
from typing import Union, Tuple, List


def compute_gain_amplification(gain: int) -> float:
    """Returns the actual amplification of intensity of gain setting."""
    arr1 = np.linspace(1, 2, 26)
    arr2 = np.linspace(2.08, 4, 25)
    arr3 = np.linspace(4.24, 10, 25)
    arr4 = np.linspace(10.56, 24, 25)
    arr = np.concatenate((arr1, arr2, arr3, arr4))
    return arr[int(gain)]


def tT_mean(tT: npt.NDArray[np.float64]) -> float:
    """Returns the average tT across all pixels in the entire image."""
    return np.mean(np.mean(tT, axis=2), axis=1)


def plot_fft(data: npt.ArrayLike, stepsize: float) -> None:
    """Plots the FFT of an Array.

    Args:
        data: Input array with data, can be complex.
        stepsize: Sample spacing (inverse of sampling rate).
    """
    freqs = 360 * np.fft.fftfreq(data.size, stepsize)
    fourier_data = np.abs(np.fft.fft(data))

    index = np.argsort(freqs)
    plt.stem(
        freqs[index], fourier_data[index], "b", markerfmt=" ", basefmt="-b"
    )
    plt.xlabel("Freq")
    plt.ylabel("FFT Amplitude |(freq)|")
    plt.title("Fourier Transform")


def get_optical_activity(
    data: npt.ArrayLike, stepsize: float
) -> Tuple[
    npt.NDArray[np.float64],
    npt.NDArray[np.float64],
    npt.NDArray[np.float64],
    npt.NDArray[np.float64],
]:
    """Computes the birefringence and absorption contrast using rFFT.

    Args:
        data: Input array with tT data, can be complex.
            Must be integer number of 360 degrees periods.
        stepsize: Sample spacing (inverse of sampling rate).

    Returns:
        birefringence: Array of birefringence contrast.
        absorption: Array of absorption contrast.
        birefringence_phase: Array of phase in birefringence (in degrees).
        absorption_phase: Array of phase in absorption (in degrees).

    """
    freqs = 360 * np.fft.rfftfreq(data.shape[0], stepsize)  # type: ignore
    fourier_data = np.fft.rfft(data, axis=0)
    fourier_abs = np.abs(fourier_data)

    mean = fourier_abs[np.where(np.isclose(freqs, 0))]
    birefringence = 2 * fourier_abs[np.where(np.isclose(freqs, 4))] / mean
    absorption = 2 * fourier_abs[np.where(np.isclose(freqs, 2))] / mean
    birefringence_phase = np.angle(
        fourier_data[np.where(np.isclose(freqs, 4))], deg=True
    )
    absorption_phase = np.angle(
        fourier_data[np.where(np.isclose(freqs, 2))], deg=True
    )
    # Restrict to phases of 0-90 and 0-180 respectively
    birefringence_phase %= 90
    absorption_phase %= 180
    return (
        birefringence[0],
        absorption[0],
        birefringence_phase[0],
        absorption_phase[0],
    )


def align_polarizers(
    polarizer: Motor,
    analyzer: Motor,
    cross: bool = False,
    home: bool = True,
) -> None:
    """Moves the polarizes into their desired configurations.

    Args:
        polarizer : Polarizer object.
        analyzer : Analyzer object.
        cross: If True, polarizer and analyzer alignment should be crossed.
           If False, they will be parallel.
        home: If True, homes both polarizers. If False, do nothing.
    """
    if home:
        polarizer.move_home()
        analyzer.move_home()
        # wait
        motion_flag = True
        while motion_flag:
            motion_flag = polarizer.is_in_motion or analyzer.is_in_motion

    # move to cross or parallel position depending on flag
    polarizer.move_to(0)
    analyzer_cross = 90 if cross else 0
    analyzer.move_to(analyzer_cross - 7.1)

    # wait again
    motion_flag = True
    while motion_flag:
        motion_flag = polarizer.is_in_motion or analyzer.is_in_motion


def exposure_setup(
    polarizer: Motor,
    analyzer: Motor,
    cam: uc480.UC480_Camera,
    cross: bool = False,
    home: bool = True,
) -> Tuple[float, int]:
    """Run an interactive test for users to select acceptable exposure parameters.

    Users can interactively change the gain and exposure time of image until they
    find a configuration that yields an acceptable exposure.

    Args:
        polarizer: Polarizer object.
        analyzer: Analyzer object.
        cam: Camera object.
        cross: If True, polarizer and analyzer alignment should be crossed.
           If False, they will be parallel.
        home: If True, homes both polarizers. If False, do nothing.

    Returns:
        exposure_time: Exposure time in seconds.
        gain: Gain setting (1-100).
    """

    # set default gain that can be updated by user
    default_gain = 100  # gain of 100 has lowest noise
    gain = default_gain
    time_units_scale = 1
    time_units_string = ""
    if cross:
        polarizer_alignment = "Cross"
        time_units_scale = 1e-3
        time_units_string = "milliseconds"
    else:
        polarizer_alignment = "Parallel"
        time_units_scale = 1e-6
        time_units_string = "microseconds"

    print(f"{polarizer_alignment} Polarized - Ask user for Exposure value")

    align_polarizers(polarizer, analyzer, cross=cross, home=home)

    while True:
        # ask user for exposure and gain
        vals = input(
            f"Enter Exposure value ({time_units_string}) *hit spacebar* then "
            + "enter gain value. Leave gain blank for default value: "
        ).split()

        if not vals:  # equivalent to len(vals) < 1
            print("Error. Invalid input.")
            continue

        try:
            try_exposure = float(vals[0])
            if len(vals) > 1:
                gain = int(vals[1])
        except ValueError:
            print("Error. Must be integer input.")
            continue

        # convert to seconds
        try_exposure *= time_units_scale
        # take image
        image_matrix = cam.grab_image(
            exposure_time=try_exposure * u.s, gain=gain
        ).astype(np.int16)

        # plot figure
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5))

        image = ax1.imshow(image_matrix[:, :, 1])
        fig.colorbar(image, ax=ax1)
        ax1.set_title("Sample x")

        data, bins = np.histogram(image_matrix[:, :, 1], 50)

        ax2.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
        ax2.bar(bins[1:], data, width=4)
        ax2.set_xlabel("Pixel Intensity")
        ax2.set_ylabel("Number of pixels")
        ax2.set_title("Pixel Intensity Histogram")

        plt.show()

        # update user on current exposure and gain value just as a reminder
        print_exp_val = np.round(try_exposure / time_units_scale)

        def get_overexposed(image_matrix: np.ndarray):
            overexposed = np.array(image_matrix >= 250, dtype=int)
            return overexposed

        overexposed = get_overexposed(image_matrix)

        print(
            f"Currently configuring exposure for {polarizer_alignment} Polarized."
        )
        print("Current exposure value is: ", print_exp_val, time_units_string)
        print("Current gain value is: ", gain)
        print(
            "Overexposed pixels: {} - {:0.4f} %".format(
                np.sum(overexposed),
                np.sum(overexposed) / overexposed.size * 100,
            )
        )

        # ask user if the values are good
        finished = input("Is this a good exposure time? (y/N): ")
        if finished.upper() == "Y":
            return try_exposure, gain


def measure(
    polarizer: Motor,
    analyzer: Motor,
    cam: uc480.UC480_Camera,
    exp: float,
    gain_val: int,
    pos: List[Union[int, str]],
    sample: str,
    p_val: str,
    cross: bool = False,
) -> str:
    """
    Performs all necessary measurements.

    Args:
        polarizer: Polarizer object.
        analyzer: Analyzer object.
        cam: Camera object.
        exp: Exposure time in seconds.
        gain_val: Gain setting for camera, ranges from 1-100.
        pos: List of motor positions [x, y, z].
        sample: Sample name.
        p_val: Point number of sample.
        cross: If True, polarizer and analyzer alignment should be crossed.
           If False, they will be parallel.

    Returns:
        csv_path: Absolute path of the csv file with image reference data.
    """
    dis.clear_output()
    if cross:
        print("Cross Polarized - Starting Measurement")
    else:
        print("Parallel Polarized - Starting Measurement")

    # define dictionary to save data
    df = {
        "Polarizer Angle": [],
        "Polarizer Angle - Setting": [],
        "Analyzer Angle": [],
        "Analyzer Angle - Setting": [],
        "X Translation": [],
        "Y Translation": [],
        "Z Translation": [],
        "Power [W]": [],
        "Power_STD [W]": [],
        "Total Intensity": [],
        "Folder": [],
        "Image Filename": [],
        "datetime": [],
        "Exposure": [],
        "Gain": [],
    }

    p_val = str(p_val)
    file_align = "cr" if cross else "al"
    align = "cross" if cross else "align"

    if cross:
        cam.start_live_video(framerate=1 * u.hertz)
        cam.stop_live_video()
        # take a 'blank' image to wait for framerate to change.
        image_matrix_temp = cam.grab_image(
            exposure_time=exp * u.s, gain=gain_val
        ).astype(np.uint16)
        image_matrix_temp = None
    else:
        cam.start_live_video(framerate=30 * u.hertz)
        cam.stop_live_video()

    align_polarizers(polarizer, analyzer, cross=cross)

    time.sleep(3)

    # Some filename definitions
    date_str = date.today().isoformat().replace("-", "")

    # FIXME: what is /pol/ folder
    image_folder = "D:/data/{}/{}/pol/{}/p{}/".format(
        date_str[0:4],
        date_str,
        sample,
        p_val,
    )
    dataframe_head = image_folder + "df_{}_{}_p{}_".format(
        file_align,
        sample,
        p_val,
    )

    dataframe_body = date_str
    dataframe_foot = ".csv"

    # return csv file path for use in another function below
    csv_path = dataframe_head + dataframe_body + dataframe_foot

    image_head = image_folder + "img_"
    image_foot = ".tiff"

    # make folder if doesn't exist
    if not os.path.exists(image_folder):
        os.makedirs(image_folder)

    starttime = time.time()

    # initialize arrays
    polarizer_pos = np.arange(0, 2 * 185, 10)
    analyzer_pos = (
        np.arange(0, 2 * 185, 10) + 90 - 7.1
        if cross
        else np.arange(0, 2 * 185, 10) - 7.1
    )

    print("Cross Polarized") if cross else print("Parallel Polarized")

    # loop through all values
    for polarizer_angle, analyzer_angle in zip(polarizer_pos, analyzer_pos):
        polarizer.move_to(polarizer_angle)
        analyzer.move_to(analyzer_angle)

        motion_flag = True
        while motion_flag:
            motion_flag = polarizer.is_in_motion or analyzer.is_in_motion

        # power_mean, power_std = averaged_power(power_meter, N_samples=20)
        power_mean, power_std = 0, 0

        def hdr() -> np.ndarray:
            """Performs an hdr image capture using multiple exposures."""
            image_matrix: npt.NDArray[np.uint16] = cam.grab_image(
                exposure_time=exp * u.s, gain=gain_val
            ).astype(np.uint16)[..., 1]

            # Overexposed pixel compensation
            exposure_scale = 1
            iteration_counter = 0
            overexposed_matrix_bool = image_matrix >= 230
            while np.sum(overexposed_matrix_bool):
                if iteration_counter > 10:
                    break
                if np.sum(overexposed_matrix_bool) > 10000:
                    exposure_scale *= 2
                    if np.sum(overexposed_matrix_bool) > 100000:
                        exposure_scale *= 2
                if exp / exposure_scale < 4e-5:  # Min exposure time
                    # TODO: Fix nan conversion in image
                    # image_matrix[overexposed_matrix_bool] = np.nan
                    print("HDR Error - Reached minimum exposure time")
                    break

                exposure_scale *= 2  # halves exposure each iteration
                iteration_counter += 1
                image_matrix_underexposed = cam.grab_image(
                    exposure_time=exp / exposure_scale * u.s, gain=gain_val
                ).astype(np.uint16)[..., 1]

                image_matrix[overexposed_matrix_bool] = (
                    image_matrix_underexposed[overexposed_matrix_bool]
                    * exposure_scale
                )
                overexposed_matrix_bool = image_matrix_underexposed >= 230
                print(
                    f"HDR iteration {iteration_counter}: {np.sum(overexposed_matrix_bool)} pixels overexposed"
                )

            # For limit cross exposure at 100*align test
            if cross:
                return image_matrix

            # Underexposed pixel compensation
            exposure_scale = 1
            iteration_counter = 0
            underexposed_matrix_bool = image_matrix <= 20

            while np.sum(underexposed_matrix_bool):
                if iteration_counter > 10:
                    image_matrix[image_matrix < 1] = 1
                    break
                if np.sum(underexposed_matrix_bool) > 10000:
                    exposure_scale *= 2
                    if np.sum(underexposed_matrix_bool) > 100000:
                        exposure_scale *= 2

                exposure_scale *= 2  # halves exposure each iteration
                iteration_counter += 1
                # Catch camera timeout error when exposure is too long
                try:
                    image_matrix_overexposed = cam.grab_image(
                        exposure_time=exp * exposure_scale * u.s, gain=gain_val
                    ).astype(np.uint16)[..., 1]
                except errors.TimeoutError:
                    print("TimeoutError. Maximum exposure time reached.")
                    image_matrix[image_matrix < 1] = 1
                    break

                image_matrix[underexposed_matrix_bool] = (
                    image_matrix_overexposed[underexposed_matrix_bool]
                    / exposure_scale
                )
                underexposed_matrix_bool = image_matrix_overexposed <= 20
                print(
                    f"HDR iteration {iteration_counter}: {np.sum(underexposed_matrix_bool)} pixels underexposed"
                )

            return image_matrix

        image_matrix = hdr()

        filename = str(
            image_head
            + str(pos[0])
            + "_P_"
            + str(polarizer_angle)
            + "_%s_"
            + "p%s"
            + image_foot
        ) % (align, p_val)

        # if pillow doesn't work, try imageIO
        im = Image.fromarray(image_matrix)
        im.save(filename)
        #         imageio.imwrite(filename, image_matrix[:, :, 1].astype(np.uint16))
        print(filename)

        datetime = pd.Timestamp.now()

        df["Polarizer Angle"].append(polarizer.position)
        df["Polarizer Angle - Setting"].append(polarizer_angle)
        df["Analyzer Angle"].append(analyzer.position)
        df["Analyzer Angle - Setting"].append(analyzer_angle)
        df["X Translation"].append(pos[0])
        df["Y Translation"].append(pos[1])
        df["Z Translation"].append(pos[2])
        df["Power [W]"].append(power_mean)
        df["Power_STD [W]"].append(power_std)
        # df['Total Intensity'].append('none')
        df["Total Intensity"].append(np.sum(image_matrix[..., 1]))
        df["Folder"].append(image_folder)
        df["Image Filename"].append(filename)
        df["datetime"].append(datetime)
        df["Exposure"].append(exp)
        df["Gain"].append(gain_val)

        pd.DataFrame(df).to_csv(csv_path)

        print("...at polarizer setting " + str(polarizer_angle))
        print("...at analyzer setting " + str(analyzer_angle))
        collapsedtime = time.time()
        print("Time elapsed: " + str(collapsedtime - starttime))
        dis.clear_output(wait=True)

    if cross:
        print("Cross Polarized - Done Measurement")
    else:
        print("Parallel Polarized - Done Measurement")

    return csv_path


# FIXME: what is this function name???
# TODO: create detailed docstring
def visvis_pd_v3(
    dir_name_al: str,
    dir_name_cr: str,
    save_flag: bool,
    sample_name: str,
    sample_point: str,
    xstart: int = 0,
    xstop: int = 1216,
    ystart: int = 0,
    ystop: int = 1936,
    pxl_size: float = 0.193,
) -> None:
    """Data analysis and plotting function."""

    str_end = dir_name_cr.find("df")
    dir_name = dir_name_al[:str_end]

    os.chdir(dir_name)
    data_cross = pd.read_csv(dir_name_cr)
    data_align = pd.read_csv(dir_name_al)

    save_path = dir_name + "anal/"

    if not os.path.exists(save_path):
        os.mkdir(save_path)

    save_path = dir_name + "anal/" + sample_name + "_p" + sample_point + "_"

    fl_name_lg = data_align["Image Filename"].shape[0]
    polarizer_angles = np.array(data_align["Polarizer Angle - Setting"])

    ims_t_cr = np.zeros([fl_name_lg, 1216, 1936])
    ims_t_al = np.zeros([fl_name_lg, 1216, 1936])

    for i in range(fl_name_lg):
        temp_cr = data_cross["Image Filename"][i].split("/")
        temp_al = data_align["Image Filename"][i].split("/")

        gain_cr = compute_gain_amplification(data_cross["Gain"][i])
        gain_al = compute_gain_amplification(data_align["Gain"][i])

        exposure_cr = data_cross["Exposure"][i]
        exposure_al = data_align["Exposure"][i]

        ims_t_cr[i, :, :] = np.array(io.imread(temp_cr[-1])) / (
            exposure_cr * gain_cr
        )
        ims_t_al[i, :, :] = np.array(io.imread(temp_al[-1])) / (
            exposure_al * gain_al
        )

    # extent will set the x and y axis of the imshow images
    extent = np.array([0.0, np.shape(ims_t_cr)[2], np.shape(ims_t_cr)[1], 0.0])
    extent *= pxl_size

    # ticks for image axis
    xticks = np.round(np.linspace(0, extent[1], 12))
    yticks = np.round(np.linspace(0, extent[2], 12))

    def config_axis() -> None:
        """Convenience function for mapping plot scaling."""
        plt.xlabel(r"X Position ($\mu m$)", fontsize=18)
        plt.ylabel(r"Y Position ($\mu m$)", fontsize=18)
        plt.xticks(xticks)
        plt.yticks(yticks)

    tT = ims_t_cr / (ims_t_cr + ims_t_al)

    Vis = (np.amax(tT, axis=0) - np.amin(tT, axis=0)) / (
        np.amax(tT, axis=0) + np.amin(tT, axis=0)
    )

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        np.std(ims_t_al, axis=0)[xstart:xstop, ystart:ystop], extent=extent
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name + "_p" + sample_point + " " + "aligned STD",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "align_std.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        np.std(ims_t_cr, axis=0)[xstart:xstop, ystart:ystop], extent=extent
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name + "_p" + sample_point + " " + "crossed STD",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "cross_std.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(Vis[xstart:xstop, ystart:ystop], extent=extent)
    config_axis()
    plt.title(
        sample_name
        + "_p"
        + sample_point
        + " "
        + "Visibility, (max-min)/(max+min)",
        fontsize=20,
    )
    plt.colorbar()
    if save_flag:
        plt.savefig(save_path + "cross_vis.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.plot(polarizer_angles, tT[:, 600, 100], ".--", label="(600,100)")
    plt.plot(polarizer_angles, tT[:, 10, 100], ".--", label="(10,100)")
    plt.plot(polarizer_angles, tT_mean(tT), ".-", linewidth=3, label="mean")
    plt.title(
        sample_name
        + "_p"
        + sample_point
        + " "
        + "Intensity norm for point (600,100) and (10,100)",
        fontsize=20,
    )
    plt.legend()
    if save_flag:
        plt.savefig(save_path + "intensity_oscillations.png")
    plt.show()

    stepsize = np.mean(polarizer_angles[1:] - polarizer_angles[:-1])

    plt.subplots(figsize=(18, 9))
    plot_fft(tT_mean(tT[:-1]), stepsize=stepsize)
    plt.title(
        sample_name + "_p" + sample_point + " " + "FFT of Average tT",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "fft.png")
    plt.show()

    optical_activity = get_optical_activity(tT[:-1], stepsize)

    (
        birefringence,
        absorption,
        birefringence_phase,
        absorption_phase,
    ) = optical_activity

    absorption_phase[absorption < 0.3] = np.nan
    birefringence_phase[birefringence < 0.3] = np.nan

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        birefringence[xstart:xstop, ystart:ystop],
        extent=extent,  # vmin=0, vmax=1
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name
        + "_p"
        + sample_point
        + " "
        + r"birefringence contrast (2a_4/a_0) ($90^\circ$ periodic)",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "birefringence.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        birefringence_phase[xstart:xstop, ystart:ystop],
        extent=extent,  # vmin=0, vmax=1
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name
        + "_p"
        + sample_point
        + " "
        + r"birefringence phase angle ($90^\circ$ periodic)",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "birefringence_angle.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        absorption[xstart:xstop, ystart:ystop],
        extent=extent,  # vmin=0, vmax=1
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name
        + "_p"
        + sample_point
        + " "
        + r"absorption contrast (2a_2/a_0) ($180^\circ$ deg periodic)",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "absorption.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        absorption_phase[xstart:xstop, ystart:ystop],
        extent=extent,  # vmin=0, vmax=1
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name
        + "_p"
        + sample_point
        + " "
        + r"absorption phase angle ($180^\circ$ deg periodic)",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "absorption_angle.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(ims_t_al[0, xstart:xstop, ystart:ystop], extent=extent)
    config_axis()
    plt.colorbar()
    plt.title(sample_name + "_p" + sample_point + " " + "aligned", fontsize=20)
    if save_flag:
        plt.savefig(save_path + "align.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(ims_t_cr[0, xstart:xstop, ystart:ystop], extent=extent)
    config_axis()
    plt.colorbar()
    plt.title(sample_name + "_p" + sample_point + " " + "cross", fontsize=20)
    if save_flag:
        plt.savefig(save_path + "cross.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow((np.std(tT, axis=0)[xstart:xstop, ystart:ystop]), extent=extent)
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name + "_p" + sample_point + " " + "crossed norm STD",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "cross_norm_std.png")
    plt.show()

    plt.subplots(figsize=(18, 9))
    plt.imshow(
        np.log(np.std(tT, axis=0)[xstart:xstop, ystart:ystop]), extent=extent
    )
    config_axis()
    plt.colorbar()
    plt.title(
        sample_name + "_p" + sample_point + " " + "log of crossed norm STD",
        fontsize=20,
    )
    if save_flag:
        plt.savefig(save_path + "log_cross_norm_std.png")
    plt.show()

    if save_flag:  # Saves the numpy array as binary - open with np.load(file)
        with open("anal/" + "tT_mean.npy", "wb") as f:
            np.save(f, tT_mean(tT))
        with open("anal/" + "visibility.npy", "wb") as f:
            np.save(f, Vis)
        with open("anal/" + "align_std.npy", "wb") as f:
            np.save(f, np.std(ims_t_al, axis=0))
        with open("anal/" + "cross_std.npy", "wb") as f:
            np.save(f, np.std(ims_t_cr, axis=0))
        with open("anal/" + "birefringence.npy", "wb") as f:
            np.save(f, birefringence)
        with open("anal/" + "absorption.npy", "wb") as f:
            np.save(f, absorption)


def extract_data(csv_path: str) -> npt.NDArray[np.float64]:
    """Returns an array of image data from list of images in csv_path."""
    str_end = csv_path.find("df")
    dir_name = csv_path[:str_end]

    os.chdir(dir_name)
    data = pd.read_csv(csv_path)

    fl_name_lg = data["Image Filename"].shape[0]
    intensity_array = np.zeros([fl_name_lg, 1216, 1936])

    for i in range(fl_name_lg):
        temp = data["Image Filename"][i].split("/")

        gain = compute_gain_amplification(data["Gain"][i])
        exposure = data["Exposure"][i]

        intensity_array[i, :, :] = np.array(io.imread(temp[-1])) / (
            exposure * gain
        )

    return intensity_array
