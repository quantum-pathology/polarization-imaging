# Imaging

* [Overview](#overview)
* [System requirements](#system-requirements)
* [Software requirements](#software-requirements)
* [Installation](#installation)
* [Usage](#usage)
* [Troubleshooting](#troubleshooting)
* [Contact](#contact)


## Overview

**Imaging** is a repository of python and jupyter files used to operate
the pathology imaging setup in RAC2 1121. This is an experimental setup using 
polarimetry to analyze tumorous tissue. Please see 
[logbook](https://git.uwaterloo.ca/medical-tissue-imaging/logbook/-/blob/main/logbook.pdf) 
for more details on current progress. 


## System Requirements

- **Windows 10**. Windows 11 is not tested. 
- Plenty of storage for raw image files (approx. 0.5GB per scan).
- At least 8GB of ram, 16GB ideally.

## Software Requirements

- A working version of `git`. This can be installed from 
[here](https://git-scm.com/download/win). 
- `Python: >= 3.7, <=3.10`
    - Non-conda version is recommended, installed from 
    [python.org](https://www.python.org/downloads/). 
    -  The Thorlabs camera (DCC3260C) does not work with newer versions of 
    python (i.e., Python3.11).
- Thorlabs legacy APT software, downloaded 
[here](https://www.thorlabs.com/software_pages/ViewSoftwarePage.cfm?Code=APT).
Make sure you select the 64bit option (unless you are on a very old 32bit
machine, in which case you'll have many other issues...)

## Installation

Inside the code blocks are all commands that can be run in your favourite shell. 
Powershell or git-bash are recommended. 

Clone the Repository. Note that you must have credentials to access to this 
repository:

    git clone https://git.uwaterloo.ca/medical-tissue-imaging/imaging.git

Create a virtual Python environment:

    cd imaging
    python -m venv .venv

Activate the virtual environment. This is the command to use whenever you want 
to invoke this specific virtual environment in the terminal:

    .venv\Scripts\activate

You should now see `(.venv)` preceding the working directory in the terminal. 

Install required packages:

    pip install -r requirements.txt

To complete installation for the thorlabs_apt package, you must locate the 
`APT.dll` in "APT installation path\APT Server". Typically this can be found in:
`C:\Program Files\Thorlabs\APT\APT Server\APT.dll`. Copy the file into your 
working folder or in `.venv\Lib\site-packages\thorlabs_apt`. Assuming it is in
the default installation location, you can run the following:

    cp 'C:\Program Files\Thorlabs\APT\APT Server\APT.dll' '.venv\Lib\site-packages\thorlabs_apt\APT.dll'

Congratulations! You have completed the installation. Now you can open your 
favourite editor, and start running experiments! Just don't forget to select 
the virtual environment as the python kernel.

### Troubleshooting Installation

#### Execution Policy Error

If you get this error while trying to activate the venv, run in an elevated
privileges Powershell (as admin):

    Set-ExecutionPolicy RemoteSigned

#### Cannot import name 'getargspec' from 'inspect'

If you are using Python3.11 (not supported, YMMV), then you must navigate to 
`.venv\Lib\site-packages\instrumental\drivers\util.py`
and replace all occurrences of `getargspec` with `getfullargspec`. Save the file
and restart the kernel, this should fix the error.

#### Module 'visa' has no attribute 'ResourceManager'

Navigate to `.venv\Lib\site-packages\instrumental\drivers\__init__.py` and
replace all instances of `import visa` with `import pyvisa as visa`. Save the 
file and close it. Restart the kernel and rerun the required cells. 

## Usage

Within the imaging directory, you will find:
* [.venv](.venv/) folder, containing the python executable and packages
* [src](src/) folder, containing the python and jupyter files to run the 
experiments as well as various analysis notebooks.

The main file that you will use is 
[ImagingProper.ipynb](src/ImagingProper.ipynb).
This file contains all needed cells to run the experiment properly.

Another file you might want to work with is 
[experimental_tools.py](src/experimental).
This module contains the functions used in 
[ImagingProper.ipynb](src/ImagingProper.ipynb).
If you want to modify this module, you must restart the kernel and reimport it
for changes to be in effect. Simply running the import statement without 
restarting the kernel will not work.


### ImagingProper.ipynb

The main file you will be working with to operate the polarization imaging 
setup.

#### Setup

These are the program setup cells. They include imports, loading of drivers and 
libraries, and custom functions. Run these cells when first starting up the 
program and after restarting the kernel. If you get the message 
`the kernel has died` while running imports, see [this](#the-kernel-has-died).

#### Please put cursor and click inside the cell below and "Run selected cell and all below"

This is the primary section to run the experiment and to collect measurements.
Simply execute all cells below (as the title suggests) and follow the on-screen
instructions. The general procedure is as follows:

1. The camera will initialize. If you get `UC480Error`, see [this](#uc480error).
2. If `focus_flag is True`, there will be on screen focusing options. You may
need to scroll down to see the live-feed from the camera.
3. After focusing, you will be prompted for the stages' `x`, `y`, `z` positions.
4. Then, the program will ask for the exposure aligned exposure time (in 
microseconds). Don't worry if you don't get it just right on your first try, you
can try different exposure values. Simply press `n` when prompted if the 
exposure is acceptable. If the exposure looks good, press `y` and the program
will proceed. The cross exposure is set automatically to 100x the aligned 
exposure time.
5. You will be prompted for the sample name and the point number.
6. The measurements will begin. Wait about 5 minutes, and the analysis plots
should be available at the bottom of the program.


[//]: # (//TODO: Troubleshooting Section)

## Troubleshooting

### The kernel has died

This is a common error caused by the Thorlabs APT motors. If they are not 
properly managed and closed, there will be a conflict with the Kinesis drivers.
To fix:

1. Open `Kinesis` application
2. Click Connect and load all available motors - you may need to do this 
multiple times to ensure all of the motors are loaded
3. Disconnect all devices
4. Restart jupyter kernel
5. Run `Setup` cells - it should now work correctly

To prevent this problem in the future, make sure to run `apt.core._cleanup()` 
before restarting or killing the kernel.
### UC480Error

This is raised whenever the thorlabs camera fails to initialize. Most likely, 
this is because the camera is already loaded by another program (python or 
thorcam software). Simply close the camera in that program (either by running
`cam.close` or exiting the camera software), then rerun the cell and it should
work. No need to restart the kernel. 

## Contact

You can reach me at [tbkhoo@uwaterloo.ca](mailto:tbkhoo@uwaterloo.ca) if you 
have any questions or concerns.